# dbdiff

dbdiff は、2 つの DB の出力結果を比較するシェルスクリプトです。

## インストール

dbdiff ディレクトリを適当な場所にコピーします。

* dbdiff
    * sql
        * .sql files...
    * config.txt
    * dbdiff.sh
    * readme.md

## 使い方

作業ディレクトリを dbdiff にします。

### 設定ファイル編集

config.txt を編集して、2 つの DB の接続オプション (mysql コマンドのオプション) を指定します。

```
MYSQL_DB1_OPTS="-h localhost -P 3306 -u root -p acp307"

MYSQL_DB2_OPTS="-h localhost -P 3306 -u root -p acs49"
```

### スクリプト実行

あとは、dbdiff.sh を実行するだけです。sql ディレクトリにあるすべてのクエリが実行されます。

```
$ ./dbdiff.sh
```

クエリを 1 つだけ実行する場合は、引数としてファイル名を指定します。その際、sql のパスは不要です。名前だけ指定します。

```
$ ./dbdiff.sh xxxx.sql
```

### 結果確認

スクリプトの結果は、output ディレクトに出力されます。

* <sql ファイル名>-<DB1_ID>.txt (DB1 のクエリ結果の出力)
* <sql ファイル名>-<DB2_ID>.txt (DB2 のクエリ結果の出力)
* <sql ファイル名>-diff.txt (DB1 と DB2 の出力比較)

※スクリプトを実行する度に上書きされます。

## ACS4.9 DB データチェック用クエリ

### 00-records-count.sql
* 下記テーブルの有効なレコードをカウントします。
    * account
    * domain
    * vm_instance
    * vm_template
    * volumes
    * snapshots
    * networks

* **"diff の比較結果で差分なし" ならば OK**

### 01-vm.sql

削除されていないインスタンス (vm_instance) をチェックします。

#### check1

* removed is NULL のレコード数を取得します。
* **"diff の比較結果で差分なし" ならば OK**

#### check2

* 以下の項目を確認します。
    * removed に値が入っていない (NULL) こと。
* **"クエリ結果出力のレコード数が 0" ならば OK**

#### check3

* 以下の項目を確認します。
    * name に変化ないこと
    * 起動しているホストに変化ないこと
    * 起動しているゾーンに変化ないこと
    * 起動しているポットに変化ないこと
    * ha_enabled=1 になっていること
    * removed に値が入っていない (NULL) こと。
    * 使用しているテンプレートに変化ないこと
    * ハイパーバイザーが適切であること
    * 状態に変化ないこと
    * 利用しているオファリングが適切であること
    * アカウント、ドメインが適切であること
* ただし、システムテンプレートに関しては
    * SystemVM Template (vSphere) -> systemvm-vmware-4.6
    * SystemVM Template (KVM)     -> systemvm-kvm-4.6

    に変更されているようなので、旧テンプレート名でも diff 差分が出ないようになっています。(10 ～ 12 行目)

    もしこの処理が不要であれば、10 ～ 12 行目をコメントアウトし、9 行目のコメントを外してください。

* **"diff の比較結果で差分なし" ならば OK**

### 02-volumes.sql

ストレージボリューム (volumes) をチェックします。

#### check1

* レコード数を取得します。
* **"diff の比較結果で差分なし" ならば OK**

#### check2

* 以下の項目を確認します。
    * アカウント、ドメインが適切であること
    * name に変化ないこと
    * size に変化ないこと
    * state='Ready' になっていること
    * removed に値が入っていない (NULL) こと。
    * インスタンスに紐図いていること
    * 使用しているテンプレートに変化ないこと
    * 使用している PrimaryStorage に変化ないこと
    * ディスクオファリングに変化ないこと
    * volume_type が適切であること
    * ゾーンに変化ないこと
    * ポットに変化ないこと
* ただし、システムテンプレートに関しては
    * SystemVM Template (vSphere) -> systemvm-vmware-4.6
    * SystemVM Template (KVM)     -> systemvm-kvm-4.6

    に変更されているようなので、旧テンプレート名でも diff 差分が出ないようになっています。(9 ～ 11 行目)

    もしこの処理が不要であれば、9 ～ 11 行目をコメントアウトし、8 行目のコメントを外してください。

* **"diff の比較結果で差分なし" ならば OK**

### 03-snapshots.sql

ストレージスナップショット (snapshots) をチェックします。

#### check1

* レコード数を取得します。
* **"diff の比較結果で差分なし" ならば OK**

#### check2

* 以下の項目を確認します。
    * アカウント、ドメインが適切であること
    * name に変化ないこと
    * size に変化ないこと
    * status='BackedUp' になっていること
    * removed に値が入っていない (NULL) こと。
    * ゾーンに変化ないこと
    * ハイパーバイザーが適切であること
    * backup_snap_id に変化ないこと
* **"diff の比較結果で差分なし" ならば OK**

### 04-networks.sql

ネットワーク (networks) をチェックします。

#### check1

* レコード数を取得します。
* **"diff の比較結果で差分なし" ならば OK**

#### check2

* 以下の項目を確認します。
    * name に変化ないこと
    * vlan_id に変化ないこと
    * gataway, cidr に変化ないこと
    * ネットワークオファリングに変化ないこと
    * ゾーンに変化ないこと
    * guest_type が適切であること
    * removed に値が入っていない (NULL) こと。
* ただし、vlan_id は　ACS4.9 で 'vlan://' のスキーム?が付加されるようになったので、

    旧バージョンでも diff 差分が出ないようになっています。(8 ～ 10 行目)

* **"diff の比較結果で差分なし" ならば OK**

### 05-vm-template.sql

テンプレート, ISO (vm_template) をチェックします。

#### check1

* レコード数を取得します。
* **"diff の比較結果で差分なし" ならば OK**

#### check2

* 以下の項目を確認します。
    * アカウントが適切であること
    * format が適切であること
    * ハイパーバイザーが適切であること
* **"diff の比較結果で差分なし" ならば OK**

### 06-domain.sql

ドメイン (domain) をチェックします。

#### check1

* レコード数を取得します。
* **"diff の比較結果で差分なし" ならば OK**

#### check2

* 以下の項目を確認します。
    * name に変化ないこと
    * removed に値が入っていない (NULL) こと。
* **"diff の比較結果で差分なし" ならば OK**

### 07-account.sql

アカウント (account) をチェックします。

#### check1

* レコード数を取得します。
* **"diff の比較結果で差分なし" ならば OK**

#### check2

* 以下の項目を確認します。
    * account_name に変化ないこと
    * domain_id に変化ないこと
    * removed に値が入っていない (NULL) こと。
* **"diff の比較結果で差分なし" ならば OK**

### 08-schem.sql

スキーマ (information_schema.columns) をチェックします。

#### check1

* レコード数を取得します。
* **"diff の比較結果で差分なし" ならば OK**

#### check2

* 以下の項目を確認します。
    * MySQLが保持するすべてのデータベースのスキーマ差分を確認 
* **"diff の比較結果で差分なし" ならば OK**
* **"diff の比較結果で差分あり" ならば バージョンアップに伴う差分の可能性あり**
